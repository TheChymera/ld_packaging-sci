A compiled PDF of the latest major revision can be downloaded [here](http://chymera.eu/pres/ld_foss/pres.pdf).

A video of a presentation held with [an older revision](https://bitbucket.org/TheChymera/foss/src/6b71ced7ee60?at=master) of these slides, can be seen [here](https://www.youtube.com/watch?v=jH4h7MsoyM8).

# “Free and Open Source Software”
Slides for the opening lecture of the ETHZ/UZH “Linux Days” - organized by [project21] TheAlternative.

###This document presents:

* a brief definition of Free and Open Source Software (FOSS).
* a short history of software freedom
* the major FOSS concepts, and corresponding licenses
* the advantages of FOSS
* some of the most prominent FOSS packages
* the nature of the FOSS user and developer base

###License: [Creative Commons Attribution-ShareAlike 3.0 Unported](http://creativecommons.org/licenses/by-sa/3.0/)
